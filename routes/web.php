<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\SignupController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return view('website.pages.index');
});

Route::get('/about', function () {
    return view('website.pages.about');
});

Route::get('/category', function () {
    return view('website.pages.categories');
});

Route::get('/contact', function () {
    return view('website.pages.contact');
});

Route::get('/product-detail', function () {
    return view('website.pages.product-detail');
});

Route::get('/seller-signin', function () {
    return view('website.pages.seller-signin');
});

Route::get('/seller-signup', function () {
    return view('website.pages.seller-signup');
});

Route::get('login',[SignupController::class,'sign']);
Route::get('loggedin',[SignupController::class,'login']);
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="An impressive and flawless site template that includes various UI elements and countless features, attractive ready-made blocks and rich pages, basically everything you need to create a unique and professional website.">
  <meta name="keywords" content="bootstrap 5, business, corporate, creative, gulp, marketing, minimal, modern, multipurpose, one page, responsive, saas, sass, seo, startup, html5 template, site template">
  <meta name="author" content="elemis">
  <title>Medikit</title>
  <link rel="shortcut icon" href="./assets/img/favicon.png">
  <link rel="stylesheet" href="./assets/css/plugins.css">
  <link rel="stylesheet" href="./assets/css/style.css">
  <link rel="stylesheet" href="./assets/css/colors/aqua.css">
  <link rel="preload" href="./assets/css/fonts/dm.css" as="style" onload="this.rel='stylesheet'">
</head>

<body>
  <div class="content-wrapper">
    <header class="wrapper bg-light pt-1">
      <nav class="navbar navbar-expand-lg classic transparent navbar-light">
        <div class="container flex-lg-row flex-nowrap align-items-center">
          <div class="navbar-brand w-100">
            <a href="./index.html">
              <img src="./assets/img/logo-dark.png" srcset="./assets/img/logo-dark@2x.png 2x" alt="" />
            </a>
          </div>
          <div class="navbar-collapse offcanvas offcanvas-nav offcanvas-start">
            <div class="offcanvas-header d-lg-none">
              <h3 class="text-white fs-30 mb-0">Medikit</h3>
              <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
			<ul class="navbar-nav d-lg-none">
                <li class="nav-item dropdown dropdown-mega">
                  <li class="nav-item"><a class="nav-link" href="{{url('/index')}}">Home</a>
                <li class="nav-item"><a class="nav-link" href="{{url('/category')}}">Categories</a>
                <li class="nav-item"><a class="nav-link" href="{{url('/about')}}">About</a>
                  <li class="nav-item"><a class="nav-link" href="{{url('/seller-signup')}}">List Your Shop</a>
                <li class="nav-item"><a class="nav-link" href="{{url('/contact')}}">Contact</a>
                


                 </li>
                  
                  <!--/.dropdown-menu -->
                </li>
                
              </ul>
          <div class="navbar-collapse offcanvas offcanvas-nav offcanvas-start">
            <div class="offcanvas-header d-lg-none">
              <h3 class="text-white fs-30 mb-0">Medikit</h3>
              <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body ms-lg-auto d-flex flex-column h-100">
              <ul class="navbar-nav">
                <li class="nav-item dropdown dropdown-mega">
                  <li class="nav-item"><a class="nav-link" href="{{url('/index')}}">Home</a>
                <li class="nav-item"><a class="nav-link" href="{{url('/category')}}">Categories</a>
                <li class="nav-item"><a class="nav-link" href="{{url('/about')}}">About</a>
                  <li class="nav-item"><a class="nav-link" href="{{url('/seller-signup')}}">List Your Shop</a>
                <li class="nav-item"><a class="nav-link" href="{{url('/contact')}}">Contact</a>
                


                 </li>
                  
                  <!--/.dropdown-menu -->
                </li>
                
              </ul>
              <!-- /.navbar-nav -->
              <div class="offcanvas-footer d-lg-none">
                <div>
                  <a href="mailto:first.last@email.com" class="link-inverse">info@email.com</a>
                  <br /> 00 (123) 456 78 90 <br />
                  <nav class="nav social social-white mt-4">
                    <a href="#"><i class="uil uil-twitter"></i></a>
                    <a href="#"><i class="uil uil-facebook-f"></i></a>
                    <a href="#"><i class="uil uil-dribbble"></i></a>
                    <a href="#"><i class="uil uil-instagram"></i></a>
                    <a href="#"><i class="uil uil-youtube"></i></a>
                  </nav>
                  <!-- /.social -->
                </div>
              </div>
              <!-- /.offcanvas-footer -->
            </div>
            <!-- /.offcanvas-body -->
          </div>
            <!-- /.offcanvas-body -->
          </div>
          <!-- /.navbar-collapse -->
          <div class="navbar-other ms-lg-4">
            <ul class="navbar-nav flex-row align-items-center ms-auto">
              <li class="nav-item"><a class="nav-link" data-bs-toggle="offcanvas" data-bs-target="#offcanvas-search"><i class="uil uil-search"></i></a></li>
              <li class="nav-item d-none d-md-block">
                <a href="#" class="btn btn-sm btn-primary rounded-pill" data-bs-toggle="modal" data-bs-target="#modal-signin">Sign In</a>
              </li>
              <li class="nav-item d-lg-none">
                <button class="hamburger offcanvas-nav-btn"><span></span></button>
              </li>
            </ul>
            <!-- /.navbar-nav -->
          </div>
          <!-- /.navbar-other -->
        </div>
        <!-- /.container -->
      </nav>
      <!-- /.navbar -->
      <div class="modal fade" id="modal-signin" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content text-center">
            <div class="modal-body">
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              <h2 class="mb-3 text-start">Welcome Back</h2>
              <p class="lead mb-6 text-start">Fill your email and password to sign in.</p>
              <form class="text-start mb-3" action="loggedin" method="GET">
                <div class="form-floating mb-4">
                  <input type="email" class="form-control" placeholder="Email" name="email" id="loginEmail">
                  <label for="loginEmail">Email</label>
                </div>
                <div class="form-floating password-field mb-4">
                  <input type="password" class="form-control" placeholder="Password" name="password" id="loginPassword">
                  <span class="password-toggle"><i class="uil uil-eye"></i></span>
                  <label for="loginPassword">Password</label>
                </div>
                <button type="submit" class="btn btn-primary rounded-pill btn-login w-100 mb-2">Sign In</button>
              </form>
              <!-- /form -->
              <p class="mb-1"><a href="#" class="hover">Forgot Password?</a></p>
              <p class="mb-0">Don't have an account? <a href="#" data-bs-target="#modal-signup" data-bs-toggle="modal" data-bs-dismiss="modal" class="hover">Sign up</a></p>
            </div>
            <!--/.modal-content -->
          </div>
          <!--/.modal-body -->
        </div>
        <!--/.modal-dialog -->
      </div>
      <!--/.modal -->
      <div class="modal fade" id="modal-signup" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content text-center">
            <div class="modal-body">
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              <h2 class="mb-3 text-start">Sign up to Sandbox</h2>
              <p class="lead mb-6 text-start">By Filling up this form you will be registered as a User.</p>
			  <div class="alert alert-success">
									@if(session()->has('message'))
										{{ session()->get('message') }}
									@endif
							</div>
              <form class="text-start mb-3" method="GET" action="login">
				@csrf
				<div class="row">
					<div class="col-sm-6">
						<div class="form-floating mb-4">
						  <input type="text" class="form-control" placeholder="First Name" name="fname" id="loginName">
						  <label for="loginName">First Name</label>
						  <span class="text-danger">
						  @error('fname')
							{{'message'}}
						  @enderror
						  </span>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-floating mb-4">
						  <input type="text" class="form-control" placeholder="Last Name" name="lname" id="loginName">
						  <label for="loginName">Last Name</label>
						  <span class="text-danger">
						  @error('lname')
							{{'message'}}
						  @enderror
						  </span>
						</div>
					</div>
				</div>
                <div class="form-floating mb-4">
                  <input type="email" class="form-control" placeholder="Email" name="email" id="loginEmail">
                  <label for="loginEmail">Email</label>
				  <span class="text-danger">
						  @error('email')
							{{'message'}}
						  @enderror
						  </span>
                </div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-floating password-field mb-4">
						  <input type="text" class="form-control" placeholder="Password" name="password" id="loginPassword">
						  <span class="password-toggle"><i class="uil uil-eye"></i></span>
						  <label for="loginPassword">Password</label>
						  <span class="text-danger">
						  @error('password')
							{{'message'}}
						  @enderror
						  </span>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-floating password-field mb-4">
						  <input type="password" class="form-control" placeholder="Confirm Password" name="cpassword" id="loginPasswordConfirm">
						  <span class="password-toggle"><i class="uil uil-eye"></i></span>
						  <label for="loginPasswordConfirm">Confirm Password</label>
						  <span class="text-danger">
						  @error('password')
							{{'message'}}
						  @enderror
						  </span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-floating mb-4">
						  <input type="text" class="form-control" placeholder="Contact" name="contact" id="Contact">
						  <label for="Contact">Contact</label>
						  <span class="text-danger">
						  @error('contact')
							{{'message'}}
						  @enderror
						  </span>
						</div>
					</div>
					<div class="col-sm-6">
						<a class="btn btn-primary rounded-pill btn-login w-100 mb-2">OTP</a>
					</div>
				</div>
                <button type="submit" class="btn btn-primary rounded-pill btn-login w-100 mb-2" data-toggle="modal" href="#ignismyModal">Sign Up</button>
              </form>
              <!-- /form -->
              <p class="mb-0">Already have an account? <a href="#" data-bs-target="#modal-signin" data-bs-toggle="modal" data-bs-dismiss="modal" class="hover">Sign in</a></p>
            </div>
            <!--/.modal-content -->
          </div>
          <!--/.modal-body -->
        </div>
        <!--/.modal-dialog -->
      </div>
      <!--/.modal -->
	  
      <div class="offcanvas offcanvas-top bg-light" id="offcanvas-search" data-bs-scroll="true">
        <div class="container d-flex flex-row py-6">
          <form class="search-form w-100">
            <input id="search-form" type="text" class="form-control" placeholder="Type keyword and hit enter">
          </form>
          <!-- /.search-form -->
          <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <!-- /.container -->
      </div>
      <!-- /.offcanvas -->
    </header>
    <!-- /header -->
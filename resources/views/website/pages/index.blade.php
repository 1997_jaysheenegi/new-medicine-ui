@extends('website.layout.layout')
@section('content')
    <section class="wrapper bg-light">
      <div class="container">
      

      <!-- /.Search -->
        <div class="row text-center mt-5">
              <div class="col-lg-10 col-xl-10 col-xxl-8 mx-auto">
               
                <h3 class="display-4 mb-9">Search Your Medicine</h3>
              </div>
              <!-- /column -->
            </div>
            <!-- /.row -->
          
          <section class="wrapper bg-light wrapper-border">
      <div class="container inner mb-5">
        <div class="row gx-lg-8 gx-xl-12 gy-4 gy-lg-0">
           <aside class="col-lg-12 sidebar">
            <form class="search-form">
              <div class="form-floating mb-0">
                <input id="search-form" type="text" class="form-control" placeholder="Search">
                <label for="search-form">Search</label>
              </div>
            </form>
            <!-- /.search-form -->
          </aside>
          </div>
              <!-- /column -->
            </div>
            </section>
             
  <!-- /.categories -->
        <div class="row text-center">
              <div class="col-lg-10 col-xl-10 col-xxl-8 mx-auto">
               
                <h3 class="display-4 mb-9">Disease</h3>
              </div>
              <!-- /column -->
            </div>
            <!-- /.row -->


        <div class="row grid-view gx-md-8 gx-xl-10 gy-8 gy-lg-0 mb-16 mb-md-19">
          <div class="col-md-6 col-lg-3">
            <div class="position-relative">
              <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -0.75rem; right: -0.75rem; width: 98%; height: 98%; z-index:0"></div>
              <div class="card shadow-lg">
                <figure class="card-img-top"><img class="img-fluid" src="./assets/img/avatars/t1.jpg" srcset="./assets/img/avatars/t1@2x.jpg 2x" alt="" /></figure>
                <div class="card-body px-6 py-5">
                  <h4 class="mb-1">Coriss Ambady</h4>
                  
                </div>
                <!--/.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /div -->
          </div>

          <!--/column -->
          <div class="col-md-6 col-lg-3">
            <div class="position-relative">
              <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -0.75rem; right: -0.75rem; width: 98%; height: 98%; z-index:0"></div>
              <div class="card shadow-lg">
                <figure class="card-img-top"><img class="img-fluid" src="./assets/img/avatars/t2.jpg" srcset="./assets/img/avatars/t2@2x.jpg 2x" alt="" /></figure>
                <div class="card-body px-6 py-5">
                  <h4 class="mb-1">Cory Zamora</h4>
                 
                </div>
                <!--/.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /div -->
          </div>
          <!--/column -->
          <div class="col-md-6 col-lg-3">
            <div class="position-relative">
              <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -0.75rem; right: -0.75rem; width: 98%; height: 98%; z-index:0"></div>
              <div class="card shadow-lg">
                <figure class="card-img-top"><img class="img-fluid" src="./assets/img/avatars/t3.jpg" srcset="./assets/img/avatars/t3@2x.jpg 2x" alt="" /></figure>
                <div class="card-body px-6 py-5">
                  <h4 class="mb-1">Nikolas Brooten</h4>
                  
                </div>
                <!--/.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /div -->
          </div>
          <!--/column -->
          <div class="col-md-6 col-lg-3">
            <div class="position-relative">
              <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -0.75rem; right: -0.75rem; width: 98%; height: 98%; z-index:0"></div>
              <div class="card shadow-lg">
                <figure class="card-img-top"><img class="img-fluid" src="./assets/img/avatars/t4.jpg" srcset="./assets/img/avatars/t4@2x.jpg 2x" alt="" /></figure>
                <div class="card-body px-6 py-5">
                  <h4 class="mb-1">Jackie Sanders</h4>
                  
                </div>
                <!--/.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /div -->
          </div>
          <!--/column -->
        </div>
         <div class="row grid-view gx-md-8 gx-xl-10 gy-8 gy-lg-0 mb-16 mb-md-19">
          <div class="col-md-6 col-lg-3">
            <div class="position-relative">
              <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -0.75rem; right: -0.75rem; width: 98%; height: 98%; z-index:0"></div>
              <div class="card shadow-lg">
                <figure class="card-img-top"><img class="img-fluid" src="./assets/img/avatars/t1.jpg" srcset="./assets/img/avatars/t1@2x.jpg 2x" alt="" /></figure>
                <div class="card-body px-6 py-5">
                  <h4 class="mb-1">Coriss Ambady</h4>
                  
                </div>
                <!--/.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /div -->
          </div>

          <!--/column -->
          <div class="col-md-6 col-lg-3">
            <div class="position-relative">
              <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -0.75rem; right: -0.75rem; width: 98%; height: 98%; z-index:0"></div>
              <div class="card shadow-lg">
                <figure class="card-img-top"><img class="img-fluid" src="./assets/img/avatars/t2.jpg" srcset="./assets/img/avatars/t2@2x.jpg 2x" alt="" /></figure>
                <div class="card-body px-6 py-5">
                  <h4 class="mb-1">Cory Zamora</h4>
                  
                </div>
                <!--/.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /div -->
          </div>
          <!--/column -->
          <div class="col-md-6 col-lg-3">
            <div class="position-relative">
              <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -0.75rem; right: -0.75rem; width: 98%; height: 98%; z-index:0"></div>
              <div class="card shadow-lg">
                <figure class="card-img-top"><img class="img-fluid" src="./assets/img/avatars/t3.jpg" srcset="./assets/img/avatars/t3@2x.jpg 2x" alt="" /></figure>
                <div class="card-body px-6 py-5">
                  <h4 class="mb-1">Nikolas Brooten</h4>
                 
                </div>
                <!--/.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /div -->
          </div>
          <!--/column -->
          <div class="col-md-6 col-lg-3">
            <div class="position-relative">
              <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -0.75rem; right: -0.75rem; width: 98%; height: 98%; z-index:0"></div>
              <div class="card shadow-lg">
                <figure class="card-img-top"><img class="img-fluid" src="./assets/img/avatars/t4.jpg" srcset="./assets/img/avatars/t4@2x.jpg 2x" alt="" /></figure>
                <div class="card-body px-6 py-5">
                  <h4 class="mb-1">Jackie Sanders</h4>
                 
                </div>
                <!--/.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /div -->
          </div>
          <!--/column -->
        </div>
          




          <section class="wrapper bg-light wrapper-border">


         <!-- /.categories -->
        <div class="row text-center">
              <div class="col-lg-10 col-xl-10 col-xxl-8 mx-auto">
               
                <h3 class="display-4 mb-9">Our Top Sellers</h3>
              </div>
              <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="swiper-container blog grid-view mb-18" data-margin="30" data-dots="true" data-items-xl="3" data-items-md="2" data-items-xs="1">
              <div class="swiper">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <article>
                      <figure class="overlay overlay-1 hover-scale rounded mb-6"><a href="#"> <img src="./assets/img/photos/b4.jpg" alt="" /></a>
                      
                      </figure>
                      <div class="post-header">
                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="./blog-post.html">Cosmetic Dentistry</a></h2>
                      </div>
                      <!-- /.post-header -->
                     
                      <!-- /.post-footer -->
                    </article>
                    <!-- /article -->
                  </div>
                  <!--/.swiper-slide -->
                  <div class="swiper-slide">
                    <article>
                      <figure class="overlay overlay-1 hover-scale rounded mb-6"><a href="#"> <img src="./assets/img/photos/b5.jpg" alt="" /></a>
                       
                      </figure>
                      <div class="post-header">
                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="./blog-post.html">Cosmetic Dentistry</a></h2>
                      </div>
                      <!-- /.post-header -->
                      <div class="post-footer">
                       
                        <!-- /.post-meta -->
                      </div>
                      <!-- /.post-footer -->
                    </article>
                    <!-- /article -->
                  </div>
                  <!--/.swiper-slide -->
                  <div class="swiper-slide">
                    <article>
                      <figure class="overlay overlay-1 hover-scale rounded mb-6"><a href="#"> <img src="./assets/img/photos/b6.jpg" alt="" /></a>
                        
                      </figure>
                      <div class="post-header">
                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="./blog-post.html">Cosmetic Dentistry</a></h2>
                      </div>
                      <!-- /.post-header -->
                      <div class="post-footer">
                        
                        <!-- /.post-meta -->
                      </div>
                      <!-- /.post-footer -->
                    </article>
                    <!-- /article -->
                  </div>
                  <!--/.swiper-slide -->
                  <div class="swiper-slide">
                    <article>
                      <figure class="overlay overlay-1 hover-scale rounded mb-6"><a href="#"> <img src="./assets/img/photos/b7.jpg" alt="" /></a>
                        <figcaption>
                          <h5 class="from-top mb-0">Read More</h5>
                        </figcaption>
                      </figure>
                      <div class="post-header">
                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="./blog-post.html">Morbi leo risus porta eget</a></h2>
                      </div>
                      <div class="post-footer">
                        <ul class="post-meta">
                          <li class="post-date"><i class="uil uil-calendar-alt"></i><span>7 Jan 2021</span></li>
                          <li class="post-comments"><a href="#"><i class="uil uil-file-alt fs-15"></i>Business Tips</a></li>
                        </ul>
                        <!-- /.post-meta -->
                      </div>
                      <!-- /.post-footer -->
                    </article>
                    <!-- /article -->
                  </div>
                  <!--/.swiper-slide -->
                </div>
                <!--/.swiper-wrapper -->
              </div>
              <!-- /.swiper -->
            </div>

 <!--/.Offers -->
        <div class="row">
          <div class="col-lg-9 col-xl-8 col-xxl-7 mx-auto text-center">
            
            <h3 class="display-4 mb-10">Latest Offers</h3>
          </div>
          <!-- /column -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
      <div class="container-fluid px-md-6">
        <div class="swiper-container blog grid-view mb-17 mb-md-19" data-margin="30" data-nav="true" data-dots="true" data-items-xxl="3" data-items-md="2" data-items-xs="1">
          <div class="swiper">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp10.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp11.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp12.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp13.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp14.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp15.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp16.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
            </div>
            <!--/.swiper-wrapper -->
          </div>
          <!-- /.swiper -->
        </div>
        <!-- /.swiper-container -->
      </div>

<!--/.NGO Offers -->
        <div class="row">
          <div class="col-lg-9 col-xl-8 col-xxl-7 mx-auto text-center">
            
            <h3 class="display-4 mb-10">Latest Offers</h3>
          </div>
          <!-- /column -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
      <div class="container-fluid px-md-6">
        <div class="swiper-container blog grid-view mb-17 mb-md-19" data-margin="30" data-nav="true" data-dots="true" data-items-xxl="3" data-items-md="2" data-items-xs="1">
          <div class="swiper">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp10.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp11.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp12.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp13.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp14.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp15.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
              <div class="swiper-slide">
                <figure class="rounded"><img src="./assets/img/photos/pp16.jpg" alt="" /></figure>
              </div>
              <!--/.swiper-slide -->
            </div>
            <!--/.swiper-wrapper -->
          </div>
          <!-- /.swiper -->
        </div>
        <!-- /.swiper-container -->
      </div>

 <!-- /.List your shop -->
        <div class="row">
          <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2 mx-auto text-center">
            <h2 class="fs-15 text-uppercase text-muted mb-3">INCREASE YOUR BUSINESS
</h2>
            <h3 class="display-4 mb-10 px-xl-10 px-xxl-15">Do you want to List your Shop?</h3>
          </div>
          <!-- /column -->
        </div>
        <!-- /.row -->
       
         <div class="tab-content mt-6 mt-lg-8 mb-md-9">
          <div class="tab-pane fade show active" id="tab2-1">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
              <div class="col-lg-6">
                <div class="row gx-md-5 gy-5 align-items-center">
                  <div class="col-6">
                    <img class="img-fluid rounded shadow-lg d-flex ms-auto" src="./assets/img/photos/sa13.jpg" srcset="./assets/img/photos/sa13@2x.jpg 2x" alt="" />
                  </div>
                  <!-- /column -->
                  <div class="col-6">
                    <img class="img-fluid rounded shadow-lg mb-5" src="./assets/img/photos/sa14.jpg" srcset="./assets/img/photos/sa14@2x.jpg 2x" alt="" />
                    <img class="img-fluid rounded shadow-lg d-flex col-10" src="./assets/img/photos/sa15.jpg" srcset="./assets/img/photos/sa15@2x.jpg 2x" alt="" />
                  </div>
                  <!-- /column -->
                </div>
                <!-- /.row -->
              </div>
              <!--/column -->
              <div class="col-lg-6">
                <h2 class="mb-3">CALL FOR ASSISTANCE: +012 345 6789</h2>
                <p>Steps to Follow:</p>
                <ul class="icon-list bullet-bg bullet-soft-yellow">
                  <li><i class="uil uil-check"></i>It is as easy and Simple</li>
                  <li><i class="uil uil-check"></i>Fill the Seller Register form</li>
                  <li><i class="uil uil-check"></i> Team will Contact</li>
                </ul>
                <a href="#" class="btn btn-yellow mt-2 mb-3">Seller Sign up</a>

              </div>
              <!--/column -->
            </div>
            <!--/.row -->
          </div>
          <!--/.tab-pane -->
          <div class="tab-pane fade" id="tab2-2">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
              <div class="col-lg-6 order-lg-2">
                <div class="row gx-md-5 gy-5">
                  <div class="col-5">
                    <img class="img-fluid rounded shadow-lg my-5 d-flex ms-auto" src="./assets/img/photos/sa9.jpg" srcset="./assets/img/photos/sa9@2x.jpg 2x" alt="" />
                    <img class="img-fluid rounded shadow-lg d-flex col-10 ms-auto" src="./assets/img/photos/sa10.jpg" srcset="./assets/img/photos/sa10@2x.jpg 2x" alt="" />
                  </div>
                  <!-- /column -->
                  <div class="col-7">
                    <img class="img-fluid rounded shadow-lg mb-5" src="./assets/img/photos/sa11.jpg" srcset="./assets/img/photos/sa11@2x.jpg 2x" alt="" />
                    <img class="img-fluid rounded shadow-lg d-flex col-11" src="./assets/img/photos/sa12.jpg" srcset="./assets/img/photos/sa12@2x.jpg 2x" alt="" />
                  </div>
                  <!-- /column -->
                </div>
                <!-- /.row -->
              </div>
              <!--/column -->
              <div class="col-lg-6">
                <h2 class="mb-3">Fast Transactions</h2>
                <p>Etiam porta sem malesuada magna mollis euismod. Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna.</p>
                <ul class="icon-list bullet-bg bullet-soft-green">
                  <li><i class="uil uil-check"></i>Aenean eu leo quam. Pellentesque ornare.</li>
                  <li><i class="uil uil-check"></i>Nullam quis risus eget urna mollis ornare.</li>
                  <li><i class="uil uil-check"></i>Donec id elit non mi porta gravida at eget.</li>
                </ul>
                <a href="#" class="btn btn-green mt-2">Learn More</a>
              </div>
              <!--/column -->
            </div>
            <!--/.row -->
          </div>
          <!--/.tab-pane -->
          <div class="tab-pane fade" id="tab2-3">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
              <div class="col-lg-6">
                <div class="row gx-md-5 gy-5">
                  <div class="col-6">
                    <img class="img-fluid rounded shadow-lg mb-5" src="./assets/img/photos/sa5.jpg" srcset="./assets/img/photos/sa5@2x.jpg 2x" alt="" />
                    <img class="img-fluid rounded shadow-lg d-flex col-10 ms-auto" src="./assets/img/photos/sa6.jpg" srcset="./assets/img/photos/sa6@2x.jpg 2x" alt="" />
                  </div>
                  <!-- /column -->
                  <div class="col-6">
                    <img class="img-fluid rounded shadow-lg my-5" src="./assets/img/photos/sa7.jpg" srcset="./assets/img/photos/sa7@2x.jpg 2x" alt="" />
                    <img class="img-fluid rounded shadow-lg d-flex col-10" src="./assets/img/photos/sa8.jpg" srcset="./assets/img/photos/sa8@2x.jpg 2x" alt="" />
                  </div>
                  <!-- /column -->
                </div>
                <!-- /.row -->
              </div>
              <!--/column -->
              <div class="col-lg-6">
                <h2 class="mb-3">Secure Payments</h2>
                <p>Etiam porta sem malesuada magna mollis euismod. Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna.</p>
                <ul class="icon-list bullet-bg bullet-soft-red">
                  <li><i class="uil uil-check"></i>Aenean eu leo quam. Pellentesque ornare.</li>
                  <li><i class="uil uil-check"></i>Nullam quis risus eget urna mollis ornare.</li>
                  <li><i class="uil uil-check"></i>Donec id elit non mi porta gravida at eget.</li>
                </ul>
                <a href="#" class="btn btn-red mt-2">Learn More</a>
              </div>
              <!--/column -->
            </div>
            <!--/.row -->
          </div>
          <!--/.tab-pane -->
        </div>


        <!--/.card -->
      </div>
      <!-- /.container -->
    </section>
    <!-- /section -->
  </div>
  <!-- /.content-wrapper -->
 @endsection
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SignupController extends Controller
{
    public function sign(Request $req)
	{
		/* $req->Validate([
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:10|confirmed',
			'contect'=>'required|int|min:10'
        ]);*/
		
		$fname = $req->fname;
		$lname = $req->lname;
		$email = $req->email;
		$password = $req->password;
		$cpassword = $req->cpassword;
		$contact = $req->contact;
		
		$st = DB::table('signups')
		->insert(
			[
			'fname'=>$fname,
			'lname'=>$lname,
			'email'=>$email,
			'password'=>$password,
			'cpassword'=>$cpassword,
			'contact'=>$contact
			]
		);
		if($st ==1)
			return redirect()->back()->with('message','Signup Successfully');
		else
		
			return redirect()->back()->with('message','Something went wrong,Please try again');
	}	
	
	/*public function login(Request $req)
	{
		$email = Signup::where('email',$req->input('email'))->get();
		if($email[0]->password)==$req->input('password'))
		{
			$req->session()->put('email',$user[0]->name));
			return redirect('/index');
		}
	}*/
}
